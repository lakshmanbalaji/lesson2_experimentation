* -------------------------------------------------------------------------------------------------
* Analysis of the hsb2 dataset
*--------------------------------------------------------------------------------------------------

* Import full data
import delimited "H:\Git For Clinical Research\lesson2_experimentation\hsb2.csv", clear

* Code variables
gen femalecode = .
replace femalecode = 0 if gender == "male"
replace femalecode = 1 if gender == "female"
label var femalecode "1female 0male"

encode prog, gen (progcode)
recode progcode (3 = 0) (1 = 2) (2 = 1)
label define progcodelabs 0 "vocational" 1 "general" 2 "academic"
label values progcode progcodelabs

encode race, gen (racecode)
label var racecode "1afam 2asian 3hisp 4white"

encode ses, gen (sescode)
label var sescode "1high 2low 3middle"

encode schtyp, gen(schtypcode)
label var schtypcode "1private 2public"

* regression model: full data
regress read write i.femalecode math i.progcode

*---------------------------------------------------------------------------------------------------

*-----------------------------------------------------------
* Introducing the hsb2_mar dataset
*-----------------------------------------------------------
import delimited "H:\Git For Clinical Research\lesson2_experimentation\hsb2_mar.csv", numericcols(5 6 7 8 9) clear 

* Code variables to match coding in full hsb2 dataset
gen femalecode = .
replace femalecode = 0 if female == "male"
replace femalecode = 1 if female == "female"
label var femalecode "1female 0male"

gen progcode = .
replace progcode = 0 if prog == "vocational"
replace progcode = 1 if prog == "general"
replace progcode = 2 if prog == "academic"
label define progcodelabs 0 "vocational" 1 "general" 2 "academic"
label values progcode progcodelabs
label var progcode "0voc 1gen 2academic"

gen racecode = .
replace racecode = race if race == 2 | race == 4
replace racecode = 3 if race == 1
replace racecode = 1 if race == 3
label var racecode "1afam 2asian 3hisp 4white"

gen sescode = .
replace sescode = 2 if ses == 1
replace sescode = 3 if ses == 2
replace sescode = 1 if ses == 3
label var sescode "1high 2low 3middle"

gen schtypcode = .
replace schtypcode = 1 if schtyp == 2
replace schtypcode = 2 if schtyp == 1
label var schtypcode "1private 2public"



* notice missingness (number of observations < 200 in several instances)
sum id femalecode racecode sescode schtypcode progcode read write math science
mdesc femalecode write read math progcode

* biased estimates
regress read write i.femalecode math i.progcode
outreg2 using ccbetas.docx, replace ctitle(Complete Case Analysis Coefficients)


*------------------------------------------------------------------------------
