# Lesson 2: Experimentation with Git



[toc]

Here's an image of the table of contents (clickable links don't seem to be working at this time).

![](images/29_toc.png)



## Introduction

* In this previous [write-up](https://bitbucket.org/lakshmanbalaji/lesson1_documentation/src/master/), we learned how to document our work with Git using staging and commit messages.
*  Here, we will examine how we can use Git to experiment with our work. The examples in this write-up (I use UCLA's [tutorial on multiple imputation](https://stats.oarc.ucla.edu/stata/seminars/mi_in_stata_pt1_new/) to walk you through the uses of branches in Git) are in Stata, but the general Git principles should apply regardless of the kind of statistical software you use.
* We will start afresh with a new folder, call it _lesson2_experimentation_ and save it as a subfolder within the _Git For Clinical Research_ folder on your workstation.

As usual, fire up the shell (Git Bash), and navigate to this directory.  

``` shell
$ cd /h/Git\ For\ Clinical\ Research/lesson2_experimentation

```

> To be able to follow along, just ensure you have a 'lesson2_experimentation' subfolder within a '*Git For Clinical Research*' folder, and substitute the `/h/` in the code for whatever path is present on your machine.

* Download the _hsb2_mar.csv_[^1] dataset from [here](https://stats.idre.ucla.edu/wp-content/uploads/2021/03/hsb2_mar.csv) and place it in the lesson2_experimentation folder.
* Download the _hsb2.csv_[^2] dataset from [here](https://www.openintro.org/data/index.php?data=hsb2) and place it in the lesson2_experimentation folder.

## Examine the hsb2_mar dataset

Notice that the dataset has 200 observations. We open up a do-file in Stata (called _script.do_) and code the variables like so. 

```SAS
import delimited "hsb2_mar.csv", numericcols(5 6 7 8 9) clear 

* Code variables
gen femalecode = .
replace femalecode = 0 if female == "male"
replace femalecode = 1 if female == "female"
label var femalecode "1female 0male"

gen progcode = .
replace progcode = 0 if prog == "vocational"
replace progcode = 1 if prog == "general"
replace progcode = 2 if prog == "academic"
label define progcodelabs 0 "vocational" 1 "general" 2 "academic"
label values progcode progcodelabs
label var progcode "0voc 1gen 2academic"

gen racecode = .
replace racecode = race if race == 2 | race == 4
replace racecode = 3 if race == 1
replace racecode = 1 if race == 3
label var racecode "1afam 2asian 3hisp 4white"

gen sescode = .
replace sescode = 2 if ses == 1
replace sescode = 3 if ses == 2
replace sescode = 1 if ses == 3
label var sescode "1high 2low 3middle"

gen schtypcode = .
replace schtypcode = 1 if schtyp == 2
replace schtypcode = 2 if schtyp == 1
label var schtypcode "1private 2public"
```

Now, run a regression analysis with reading score as the outcome and writing score, math score, program type and gender as predictors.

```SAS
* run regression model
regress read write i.femalecode math i.progcode
```

These are our results.

```SAS


      Source |       SS           df       MS      Number of obs   =       130
-------------+----------------------------------   F(5, 124)       =     23.69
       Model |  5895.48143         5  1179.09629   Prob > F        =    0.0000
    Residual |  6172.12627       124  49.7752118   R-squared       =    0.4885
-------------+----------------------------------   Adj R-squared   =    0.4679
       Total |  12067.6077       129  93.5473465   Root MSE        =    7.0552

------------------------------------------------------------------------------
        read |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       write |   .4410834   .0926477     4.76   0.000     .2577076    .6244592
1.femalecode |  -2.706338   1.365195    -1.98   0.050     -5.40844   -.0042351
        math |   .3210525   .0951436     3.37   0.001     .1327367    .5093682
             |
    progcode |
    general  |   .5177428   1.880833     0.28   0.784    -3.204953    4.240438
   academic  |   1.811155   1.654859     1.09   0.276    -1.464274    5.086585
             |
       _cons |    13.0265   4.123545     3.16   0.002     4.864848    21.18815
------------------------------------------------------------------------------		


```

Notice that despite the dataset having 200 observations, only 130 were used in the regression model. Why? Because the default option for the **regress** command in Stata is to perform complete-case analysis. Only the observations that had non-missing values for _read_, _write_, _femalecode_, _math_ and _progcode_ were used in the regression. As you can see below, missingness for these variables is between 4.5% to 9%.

```SAS
* summarize missingness
mdesc read write femalecode math progcode

    Variable    |     Missing          Total     Percent Missing
----------------+-----------------------------------------------
           read |           9            200           4.50
          write |          17            200           8.50
     femalecode |          18            200           9.00
           math |          15            200           7.50
       progcode |          18            200           9.00

```

You are perturbed by this, but you save the table of regression coefficients to your folder (_cc_betas.docx_) programmatically using the [user written _outreg2_ command](http://repec.org/bocode/o/outreg2.html). (You can also do this manually but I prefer the automatic route to save time and reduce errors.)

```SAS
* install outreg2 (do only once per Stata installation on your computer)
ssc install outreg2
* run outreg2
outreg2 using ccbetas.docx, replace ctitle(Complete Case Analysis Coefficients)
```

At this point, your directory has 2 datasets (_hsb2.csv_ and _hsb2_mar.csv_), one script (_script.do_) and one word doc (_cc_betas.docx_). 

![](images\1_completecaseanalysisfolder.png)

Ignore the _cc_betas.txt_ file, that is just a helper file used to create the word doc of regression coefficients. Open up Git Bash, and stage and commit your work (as a reminder, you only need to type the words that appear after the **$** sign)

```shell
$ git log --oneline

057d160 (HEAD -> master) Added hsb2_mar.csv, created do file for regression, output table of coefficients
```

> Pay attention to the words within the parentheses that occur right after the SHA-1 ID of the commit ('HEAD-> master'). The word HEAD says that the commit 057d160 is the most recent commit in this project, and the word 'master' is telling you that you are on the master branch. The implication of this will become clearer in the next section.

You realize that using just 130 cases out of a total of 200 cases may be causing biased estimates and overly inflated standard errors. You decide to try multiple imputation to get more unbiased coefficients. Since this is the first time you're trying multiple imputation, you don't want this _"experimental"_ analysis to be incorporated into the main analysis just yet. You would like to isolate the work you've done so far (i.e, the complete case analysis) from the multiple imputation you are going to do in the future.

You can do this with branches!

## Making your first experiment: the branch

You're going to create a branch called _mi_. Type this in your shell.

```shell
$ git checkout -b mi
Switched to a new branch 'mi'

$ git branch
  master
* mi
```

The `git checkout -b mi` command has three parts:

* The word **-b** is asking Git to create a new branch.
* The word **checkout** is asking Git to immediately switch into the new branch.
* The word **mi** is naming the branch so you can easily refer to it when  trying to switch into it in the future.

The `git branch` command tells you what branches currently exist in this project.  There are two branches:

* _master_ (the older branch)
* _mi_ (the branch we just created). Notice the asterisk next to the _mi_ branch. That is saying that you are currently on the _mi_ branch (it has been checked out). Any stages and commits that you now make will happen on the _mi_ branch. 

Let's make some changes to the _script.do_ file.

* Delete the lines from the bottom of the script: these are the lines that run the complete-case analysis regression model, summarize missingness and write output.
* Your script should look like this now.

```SAS
import delimited "hsb2_mar.csv", numericcols(5 6 7 8 9) clear 

* Code variables
gen femalecode = .
replace femalecode = 0 if female == "male"
replace femalecode = 1 if female == "female"
label var femalecode "1female 0male"

gen progcode = .
replace progcode = 0 if prog == "vocational"
replace progcode = 1 if prog == "general"
replace progcode = 2 if prog == "academic"
label define progcodelabs 0 "vocational" 1 "general" 2 "academic"
label values progcode progcodelabs
label var progcode "0voc 1gen 2academic"

gen racecode = .
replace racecode = race if race == 2 | race == 4
replace racecode = 3 if race == 1
replace racecode = 1 if race == 3
label var racecode "1afam 2asian 3hisp 4white"

gen sescode = .
replace sescode = 2 if ses == 1
replace sescode = 3 if ses == 2
replace sescode = 1 if ses == 3
label var sescode "1high 2low 3middle"

gen schtypcode = .
replace schtypcode = 1 if schtyp == 2
replace schtypcode = 2 if schtyp == 1
label var schtypcode "1private 2public"

```

* Stage and commit the _script.do_ file. **This is the first commit that makes your _mi_ branch diverge from the _master_ branch**. 

```shell
$ git add script.do
$ git commit -m "Deleted complete case analysis code"
```

* Now, add the code for multiple imputation. Note: this write-up does not attempt to teach the process of multiple imputation, instead just using it as a crutch to demonstrate how branches work. Your code should look like this now.

```SAS
import delimited "hsb2_mar.csv", numericcols(5 6 7 8 9) clear 

* Code variables
gen femalecode = .
replace femalecode = 0 if female == "male"
replace femalecode = 1 if female == "female"
label var femalecode "1female 0male"

gen progcode = .
replace progcode = 0 if prog == "vocational"
replace progcode = 1 if prog == "general"
replace progcode = 2 if prog == "academic"
label define progcodelabs 0 "vocational" 1 "general" 2 "academic"
label values progcode progcodelabs
label var progcode "0voc 1gen 2academic"

gen racecode = .
replace racecode = race if race == 2 | race == 4
replace racecode = 3 if race == 1
replace racecode = 1 if race == 3
label var racecode "1afam 2asian 3hisp 4white"

gen sescode = .
replace sescode = 2 if ses == 1
replace sescode = 3 if ses == 2
replace sescode = 1 if ses == 3
label var sescode "1high 2low 3middle"

gen schtypcode = .
replace schtypcode = 1 if schtyp == 2
replace schtypcode = 2 if schtyp == 1
label var schtypcode "1private 2public"

* MI begins (MICE)
mi set mlong
mi misstable patterns femalecode write read math progcode
* Imputation phase:
mi register imputed femalecode write read math progcode science
mi impute chained (logit) femalecode (mlogit) progcode (regress) write read math science = socst, ///
add(10) rseed (53421) savetrace(trace1,replace)
* Analysis phase:
mi estimate: regress read write i.femalecode math i.progcode


```

* Stage the _script.do_, and also the _trace1.dta_ file (created by the _MICE_ code) and then commit. **This is the second commit that makes your _mi_ branch diverge from the _master_ branch.**

```shell
$ git add trace1.dta script.do
$ git commit -m "Performed multiple imputation and obtained pooled estimates"
```

* Now, add a line to your _script.do_ to write the coefficients to the folder.  I use the user-written [_asdoc_](https://www.statalist.org/forums/forum/general-stata-discussion/general/1435798-asdoc-an-easy-way-of-creating-publication-quality-tables-from-stata-commands) command this time. 

```SAS
import delimited "hsb2_mar.csv", numericcols(5 6 7 8 9) clear 

* Code variables
gen femalecode = .
replace femalecode = 0 if female == "male"
replace femalecode = 1 if female == "female"
label var femalecode "1female 0male"

gen progcode = .
replace progcode = 0 if prog == "vocational"
replace progcode = 1 if prog == "general"
replace progcode = 2 if prog == "academic"
label define progcodelabs 0 "vocational" 1 "general" 2 "academic"
label values progcode progcodelabs
label var progcode "0voc 1gen 2academic"

gen racecode = .
replace racecode = race if race == 2 | race == 4
replace racecode = 3 if race == 1
replace racecode = 1 if race == 3
label var racecode "1afam 2asian 3hisp 4white"

gen sescode = .
replace sescode = 2 if ses == 1
replace sescode = 3 if ses == 2
replace sescode = 1 if ses == 3
label var sescode "1high 2low 3middle"

gen schtypcode = .
replace schtypcode = 1 if schtyp == 2
replace schtypcode = 2 if schtyp == 1
label var schtypcode "1private 2public"

* MI begins (MICE)
mi set mlong
mi misstable patterns femalecode write read math progcode
* Imputation phase:
mi register imputed femalecode write read math progcode science
mi impute chained (logit) femalecode (mlogit) progcode (regress) write read math science = socst, ///
add(10) rseed (53421) savetrace(trace1,replace)
* Analysis phase:
mi estimate: regress read write i.femalecode math i.progcode


* Output coefficients to word doc (myfile.docx)
ssc install asdoc /* install user written asdoc package */
asdoc mi estimate, replace


```

* Your folder should now have a word doc called _MyFile.docx_.  You can now remove the older files (_ccbetas.docx_ and _ccbetas.txt_), and stage and commit.   **This is the third commit that makes your _mi_ branch diverge from the _master_ branch**. 

  ```shell
  $ git rm ccbetas.docx ccbetas.txt
  $ git add Myfile.doc script.do
  $ git commit -m "Removed older coefficients from complete case analysis, printed out newer coefficients from MICE (multiple imputation analysis)"
  ```

At this stage, if you open up the _Myfile.doc_, you should see regression coefficients from your multiple imputation analysis. Your folder should look like this (has five files).

![](images\2_MICEfolder.png)

Your log looks like this.

```shell
$ git log --oneline --graph

* b6feee7 (HEAD -> mi) Removed older coefficients from complete case analysis, printed out newer coefficients from MICE (multiple imputation analysis)
* def492a Performed multiple imputation and obtained pooled estimates
* 7bf634e Deleted complete case analysis code
* 057d160 (master) Added hsb2_mar.csv, created do file for regression, output table of coefficients
```

Notice that besides the **_057d_** commit (inherited from the _master_ branch) there are now three new commits (SHA-1 commit IDs starting with _7bf, def, b6f_). These three commits exist only on the _mi_ branch. They do not exist on the _master_ branch. This _mi_ branch now holds a clean directory, with a total of four commits, uncluttered by any results from the complete case analysis. You can use this branch to present your multiple imputation results at a team meeting or to a colleague for their thoughts.

Here's what your commit graph looks like at this point. Notice the HEAD reference points to the latest commit on the _mi_ branch, this happens to be commit ID _**b6fee**_.

> Note: the commit IDs that you see on your screen will be different from what I show in this write-up. All that matters is that you notice each ID uniquely refers to a commit.

![](images/3_commitgraph.png)

Now, if the multiple imputation plan is shot down because the analysis was invalid for some reason, you can always jump right back to the _master_ branch without having to worry about deleting files or saving/moving around any documents.

```shell
$ git checkout master
```

Now, your folder looks like this (reverts back to the five files you had before beginning the multiple imputation analysis).

![](images/1_completecaseanalysisfolder.png)

It's as if the multiple imputation analysis was never attempted. This is what the commit graph looks like at this point.

![](images/4_commitgraph.png)

The greyed out commits on the _mi_ branch serve to reinforce the fact that at this stage, your _master_ branch is unaware of any of the developments that occurred on the _mi_ branch (but the branch still exists). If you run `git log`, the only commit you will see is **_057d_**, and the HEAD reference will point to it, as this commit is the most recent commit on the current _master_ branch. In a way, Git helps it seem like no multiple imputation was done, no trace file was saved, and no regression coefficients were written to Word. _**This, in my opinion, is the most attractive feature of branches. You don't have to worry about saving your experimental work in temporary folders, they can just hang out invisibly in the background. You can switch into them with just a single line of code when you need and recover all of your work**_.

> What if you just wanted your script.do in the master branch to be updated with the script.do in the mi branch? You would do **$ git checkout master** to get onto the master branch, and then just checkout the one file of interest from the mi branch by typing in **$ git checkout mi script.do**. However, the more formal way to incorporate work from branches is to use merges.



Here's another one of my cartoons summarizing why it is a good idea to use branches in Git.

![](images/28_tattoovertical.png)

##  Incorporating work from your experiments: merges

If you don't want to let work from your branches stagnate and if you want to bring them over to the master branch, you can use **_merges_**. There are two fundamental types of merges: fast-forward merges and merge-commits. Let's look at each with examples.

### Fast forward merges

You would use a fast-forward commit when you want the work done in your experimental branch to be brought over to your main branch and leave no evidence that a branch was ever created. In this instance, the branch is actually deleted and even Git cannot access it. This is useful for when you don't want to clutter up your project with a lot of branches forking in and out all over the place. The only requirement for the fast forward merge is that the main branch be frozen and not modified at all after the creation of the experimental branch.

In our example, we are currently on the _master_ branch with just one commit (Confirm this by typing in `git checkout master` and running `git log`. Let's create a new experimental branch called _ff_.

```shell 
$ git checkout -b ff
```

We are now on the _ff_ branch. Add a line to the bottom of your do-file to do a t-test comparing reading scores between men and women.

```SAS
ttest read, by(femalecode)


Two-sample t test with equal variances
------------------------------------------------------------------------------
   Group |     Obs        Mean    Std. Err.   Std. Dev.   [95% Conf. Interval]
---------+--------------------------------------------------------------------
       0 |      76    52.97368    1.148145    10.00929    50.68646    55.26091
       1 |      97    52.62887    .9947586    9.797236    50.65429    54.60345
---------+--------------------------------------------------------------------
combined |     173    52.78035    .7499082    9.863502    51.30014    54.26055
---------+--------------------------------------------------------------------
    diff |            .3448182    1.515172               -2.646031    3.335668
------------------------------------------------------------------------------
    diff = mean(0) - mean(1)                                      t =   0.2276
Ho: diff = 0                                     degrees of freedom =      171

    Ha: diff < 0                 Ha: diff != 0                 Ha: diff > 0
 Pr(T < t) = 0.5899         Pr(|T| > |t|) = 0.8202          Pr(T > t) = 0.4101

. 
```

Stage and commit.

```shell
$ git add script.do
$ git commit -m "t-test of reading by gender"
```

Add another line to your do-file to compare writing scores by gender.

```SAS

. ttest write, by(femalecode)

Two-sample t test with equal variances
------------------------------------------------------------------------------
   Group |     Obs        Mean    Std. Err.   Std. Dev.   [95% Conf. Interval]
---------+--------------------------------------------------------------------
       0 |      73    50.78082    1.212449    10.35917    48.36385     53.1978
       1 |      93    55.45161    .8067788    7.780293    53.84928    57.05395
---------+--------------------------------------------------------------------
combined |     166    53.39759    .7197155    9.272886    51.97655    54.81863
---------+--------------------------------------------------------------------
    diff |           -4.670791    1.407933               -7.450803   -1.890779
------------------------------------------------------------------------------
    diff = mean(0) - mean(1)                                      t =  -3.3175
Ho: diff = 0                                     degrees of freedom =      164

    Ha: diff < 0                 Ha: diff != 0                 Ha: diff > 0
 Pr(T < t) = 0.0006         Pr(|T| > |t|) = 0.0011          Pr(T > t) = 0.9994

. 

```

Stage and commit.

```shell
$ git add script.do
$ git commit -m "t-test of writing by gender"
```

Use the **$ git log** command to view your commits on the experimental _ff_ branch.

```shell
$ git log --oneline --graph

* c6f55c5 (HEAD -> ff) t-test of writing by gender
* dd95021 t-test of reading by gender
* 057d160 (master) Added hsb2_mar.csv, created do file for regression, output table of coefficients

```

There are a total of three commits on _ff_ (The commit **_057d_** was inherited from the _master_ branch, and then the two t-test commits _**dd95, c6f5**_ (the most recent commit, as pointed out by the HEAD reference) were added incrementally.) 

At this stage, this is what your project looks like overall.

![](images/5_ff_img1.png)

The _mi_ and the _master_ branches are greyed out because you are currently on the experimental _ff_ branch. You can confirm this by typing in `git branch` and noting the asterisk next to _ff_. Note that the greyed out _master_ branch has not moved from commit **_057d_**.

Now, if we decide that we want to incorporate the work done in the experimental branch _ff_ into the _master_ branch, we can do a **_fast-forward merge_**. We do it in three steps:

1. **Checkout the master branch**. Notice the HEAD reference points to the latest commit on _master_.

   ```shell
   $ git checkout master
   ```

   ![](images/6_ff_img2.png)

   

2. **Merge the _ff_ branch into the _master_ branch**. This copies over the commits (along with the commit IDs) from the _ff_ branch onto the _master_ branch and updates the HEAD reference on _master_ to whatever happened to be the latest commit on the _ff_ branch.

   ```shell
   $ git merge ff
   
   Updating 057d160..c6f55c5
   Fast-forward
    script.do | 15 +++++++++++++--
    1 file changed, 13 insertions(+), 2 deletions(-)
   
   ```

   ![](images/7_ff_img3.png)

   Note at this stage that the _ff_ branch still exists (as shown by the greyed out commits). You can leave it as is, but if you want a cleaner project without too many branches...

   

3. **Delete the _ff_ branch as you no longer need it. **

   The only information your experimental _ff_ branch added was the two new commits, but those have already been brought over to the _master_ branch in the previous step. The existence of the _ff_ branch is now redundant.

   ```shell
   $ git branch -d ff
   
   Deleted branch ff (was c6f55c5).
   ```

   ![](images/8_ff_img4.png)



Note that the _ff_ branch no longer exists.  The above image is a snapshot of your project 

You are now done with a fast forward merge.  

#### Squash merge

A squash commit very similar to a fast forward merge, but is less granular. It is done when you want to collapse all the commits on the experimental branch into one commit and then only bring that single commit over to the _master_ branch. Let us re-examine the exact same scenario as above, where you have two extra commits on the _ff_ branch, but imagine we decide to do a squash merge instead of a fast forward merge.

![](images/5_ff_img1.png)



To **bring the two commits over into _master_ as a single commit**, you would have four steps (as opposed to three):

1. _Checkout the master branch._

   ```shell
   $ git checkout master
   ```

   ![](images/6_ff_img2.png)

2.  _Merge the ff branch into the master branch using the --squash flag_

   ```shell
   $ git merge --squash ff
   ```

![](images/9_ff_img5.png)

Notice that the HEAD reference has not moved from commit **_057d_** on _master_. There is a potential new commit (represented by the dotted lines and circle) that holds the information from the two _ff_ commits (**_dd950_** and **_c6f55_**) but it has not yet been etched into the _master_ branch, i.e, it does not have an SHA-1 ID yet. 

3. _Complete the squash merge by adding a commit message_

   ```shell
   $ git commit -m "t-test of reading & writing"
   ```

![](images/10_ff_img6.png)

Notice that the potential commit has now been confirmed as a new commit (**_5b13f_**). Notice that its ID is totally different from the two _ff_ commits that it originated from. The HEAD reference has moved, indicating that the squash-merge commit is the latest commit on _master_. If you open up your _script.do_ now, you will find that it has both the t-test of reading scores by gender and the t-test of writing scores by gender.



4. _Delete the ff branch_

   ```shell
   $ git branch -d ff
   ```

   ![](images/11_ff_img7.png)

   

Since the information from both commits on _ff_ now exists in the **_5b13f_** commit on _master_, the _ff_ branch is redundant and can be deleted.

> Note: in order to do  fast-forward merges or squash merges, the important thing to remember is that you need to **keep the master branch frozen in time** from the point that the experimental branch is created. If you create an experimental branch, make a couple of commits on it, switch back to master and make a couple of commits on it, you can no longer fast-forward or squash merge the experimental branch onto the master branch, as the master branch has changed since the experimental branch split off. 

### Merge commits & conflicts

The above examples had two things in common:

1. The _master_ branch didn't change at all since the point at which the  experimental branch broke off. What if the _master_ branch had also changed, adding commits of its own?
2. The experimental branch was destroyed once its work was merged into _master_. What if you wanted the work done in the experimental branch to be remain, just to retain a log of the fact that a branch was once created?

In both these cases, we cannot use a fast-forward commit. We would use a _merge commit_, where the joining of the two branches becomes a commit with a message of its own, and the join is clearly representing in the log. These commits usually occur without issues (when you pass the `git merge` command, Git automatically realizes a merge commit needs to be done, executes it and prompts you to add a message). In this example, however, we are going to see what happens in a merge commit when the two branches conflict.

Make sure you are in the _master_ branch. Create a new dofile, and call it _script2.do_. Open it up and import the _hsb2.csv_ dataset (the full data without any missingness) using the following lines of code.

```SAS
1 * Import data
2   clear
3   import delimited "hsb2.csv"
```

Notice I have added numbers to the code. Line 1 has an annotation (* Import data), line 2 has a command "clear" to remove older datasets, and line 3 imports the _hsb2.csv_ file.

Stage and commit.

 ```shell 
 $ git add script2.do
 $ git commit -m "Created script2.do, imported hsb2.csv"
 ```

Your project now looks like this.

![](images/13_mc1.png)

Now, create a branch called _buckets_. 

```shell
$ git checkout -b buckets

Switched to a new branch 'buckets'
```

The idea is that at some point in the future, you want to be able to categorize all the test scores into buckets, so in this _buckets_ branch, you add a bit of code (a loop) to do this in your _script2.do_ file. This is what the do file looks like now.

```SAS
1 *Import data
2 clear
3 import delimited "hsb2.csv"
4
5
6
7 *Categorize
8 foreach v of varlist test1-test5 { # assumes you will be looping over 5 variables
9   egen bucket_`v' = cut(`v'), at(30,40,50,60,70)
10  } 

```

> Note: Observe that this do file (in the buckets branch) has 10 lines in total, with lines 4 and 5 blank.

You stage and commit. 

```shell
$ git add script2.do
$ git commit -m "Added loop to categorize all test score variables"
```

Happy to have done this, you switch back to _master_ by typing `git checkout master`. You realize all of a sudden that the loop you wrote assumes the input variables are called _test1_, _test2_, _test3_, _test4_ and _test5_. You notice that the variables are currently called _read_, _write_, _math_, _science_ and _socst_. You decide to quickly fix this, and you add a line to _script2.do_ to rename the variables, and you stage and commit. **This changes the _master_ branch and the experimental _bucket_ branch will not be aware of this**.

```SAS
1 * Import data
2   clear
3   import delimited "hsb2.csv"
4 * Rename vars for loop
5   rename (read write math science socst) (test1 test2 test3 test4 test5)

```

```shell
$ git add script2.do
$ git commit -m "score vars start with prefix test"
```

> Note: since we are on the master branch, script2.do doesn't have the loop that we wrote. This do file has only 5 lines (i.e, all the lines after line 5 are blank). This do-file has the renaming happening in lines 4 and 5.

Here's what the project looks like overall.



![](images/14_mc2.png)





You now want to bring the _master_ and the _buckets_ branch together. Remember, _master_ has changed since the _buckets_ branch broke off, so a fast forward merge is not possible, and a merge commit is going to happen. With _master_ checked out, type this.

```shell
$ git merge buckets

CONFLICT (add/add): Merge conflict in script2.do
Auto-merging script2.do
Automatic merge failed; fix conflicts and then commit the result.
```

You get an error message! What do you think happened? Open the _script2.do_ file. You see this.

```SAS
1 * Import data
2 clear
3 import delimited "hsb2.csv"
4 <<<<<<< HEAD
5 * Rename vars for loop
6 rename (read write math science socst) (test1 test2 test3 test4 test5)
7
8
9
10
11
12 =======
13
14
15
16 * Categorize
17 foreach v of varlist test1-test5 {
18   egen bucket_`v' = cut(`v'), at(30,40,50,60,70)
19  } 
20 >>>>>>> buckets


```

* The arrows and the equals signs are **_conflict markers_.**
* The portion between **<<<<< HEAD** and  **=======** has the two lines that we jammed into _script2.do_ to rename the variables in the _master_ branch. 
* The portion between  **=======** and **>>>>>>> buckets** has the loop that we added to _script2.do_ in the _buckets_ branch.

Git is basically telling us that we changed the exact same lines in _script2.do_ in two different ways in each of the two branches, and that it does not know how to bring them together. Here's an image with lines in black showing where the two branches agree and lines in red showing where the two branches disagree. There are two conflicts.

![](images/12_mergecommit_conflict1.png)

Git needs to be explicitly told how to resolve these discrepancies. We will edit the do-file making sure to remove the conflict markers.

```SAS
1 * Import data
2 clear
3 import delimited "hsb2.csv"
4 * Rename vars for loop
5 rename (read write math science socst) (test1 test2 test3 test4 test5)
6 * Categorize
7 foreach v of varlist test1-test5 {
8   egen bucket_`v' = cut(`v'), at(30,40,50,60,70)
9 } 
```

* For the first conflict about lines 4 & 5: **_By explicitly retaining the rename lines in the do-file, we ask Git to discard the input from the buckets branch, i.e, we do not want lines 4 & 5 to be blank_**

* For the second conflict about lines 7, 8, 9, 10: _**By explicitly retaining the loop lines in the do-file, we ask Git to discard the input from the master branch, i.e, we do not want lines 7, 8, 9 to be blank**_.

We now save _script2.do_, stage and commit.

```shell
$ git add script2.do
$ git commit -m "Merged the rename and loop codes"
```

This creates a new commit, the **_merge commit_** **_4cc2c_** (This would not have happened in a fast-forward merge, the merge would have happened silently without any commits).

This is what your project looks like now. Notice the _buckets_ branch is still retained, and its contribution to the _master_ branch is now crystal clear. 

![](images/15_mc3.png)

## Making sure your experiments are up to date with your main work: rebasing

I like to think of rebasing as updating an experimental branch on what's happening in the main branch. It is best understood with an example.

While on the _master_ branch, go ahead and create a file called _script3.do_.  Add these lines to the do file. We are importing hsb2.csv again, and creating an indicator variable that has the value 1 for all persons who scored above the overall median reading score, and 0 otherwise. Add and stage it.

```SAS
* Import data
clear
import delimited "hsb2.csv"

* Define high performers based on median reading score
gen high = 0
su read, det
* values of reading above median are considered high
replace high = 1 if read > r(p50) 
label define highlabs 1 "highreading" 2 "lowreading"
label values high highlabs



```

```shell
$ git add script3.do
$ git commit -m "Created script3.do and created variable 'high' (median split on read) and also labelled variable high to define 1 as high and 2 as low"
```

Now, create and checkout a branch called _graphic_.

```shell
$ git checkout -b graphic
```

We will use this branch only to create a simple graphic (incrementally) from the current dataset.   We want to graph the mean math scores for the two groups of students with high and low reading scores (high reading score defined as a score above median). Add these lines to _script3.do_ and stage and commit.

```SAS
graph math, ///
over(high) 
```

```shell
$ git add script3.do
$ git commit -m "Created basic bar graph of math score by high/low readers"
```



![](images/16_bar1.png)

Now we decide to add a title to the graph. Edit the lines that create the bar graph  in _script3.do_ and stage and commit.

```SAS
graph math, ///
over(high)  ///
title("Mean math score by reading achievement")
```

```shell
$ git add script3.do
$ git commit -m "Adding title to bar graph"
```

![](images/17_bar2.png)

In a similar iterative fashion, we decide to change the Y axis title to "Mean Math Score" in another commit, and we change the color of the bars to 'maroon' and 'grey' in a final commit. Our final code looks like this:

```SAS
graph bar math, ///
over(high)      ///
title("Mean math score by reading achievement") ///
ytitle("Mean Math score") ///
legend(order(1 "low reader" 2 "high reader")) ///
asyvars bar(1, color(maroon))  ///
        bar(2, color(gray)) 
```

The final bar graph looks like this.

![](images/18_bar3.png)



This is what our project looks like at this stage. Note specifically where the _graphic_ branch split off from the _master_ branch (_**1c645**_) and the number of commits it has (_graphic_ has 4 commits that are unique to it).

![](images/19_graphicssplit.png)



Zooming in more specifically on the _graphic_ branch...

![](images/19_graphicssplitb.png)



Now, imagine you switch back to master (`git checkout master`, and make a change to the way you define a high and low reading score in _script3.do_. Instead of defining high readers as people who score above the median reading score, you now redefine it as people who score more than the 90th percentile (`replace high = 1 if read > r(p90) `, and you stage and commit that change (commit ID _**ab446**_). Your project now looks like this. 

![](images/20_graphicssplitc.png)

The _graphic_ branch is now out of sync with your _master_ branch, because a fundamental change has happened to the input dataset that _graphic_ was using. It would be too tedious to create a new branch (_graphic2_) at _**ab446**_ because we would then have to painstakingly recreate all those commits that led to the bar graph. There has to be an easy way to bring the change in _master_ over to _graphic_. The answer is to use `git rebase`.

First, checkout graphic (`git checkout graphic`).

![](images/20_graphicssplitd.png)



Now, type this in the shell.

```shell
$ git rebase master
```

You will get a merge conflict because the _graphic_ branch insists that the line in _script3.do_ was `replace high = 1 if read > r(p50) `. Override that and manually ensure that the line is replaced by `replace high = 1 if read > r(p90) `. Save _script3.do_, then stage it using `git add script3.do`, and tell Git to continue rebasing by saying `git rebase --continue`. 

You will get a screen that looks a lot like the interactive commit message screen. Press 'i' to go into insert mode and follow the instructions (if you want) to change the commit message for commit **_1f85_(created basic bar graph)**. If you do not want to change the message, press 'i' anyway to go into insert mode, and then just press `Esc` and enter `:wq` to exit out of the interactive mode.

This is the result.

![](images/21_graphicssplite.png)

Notice now that history has been rewritten (all the commit IDs for the _graphic_ branch have changed). It looks like the _graphic_ branch split off from _master_ at a point where the _high_ variable was defined as 1 for persons scoring in the 90th percentile, and 0 for persons scoring lower than that. If you type `git checkout graphic` and run the code in _script3.do_, you will see that the bar graph that pops out is automatically updated.

![](images/22_endrebase.png)



## Cleaning up your commits: an example of interactive rebasing

Building off the rebase function, we can use the `-i` flag (for interactive) to do a number of things to change and organize our commits. We can:

1. _**Amend**_ commits 
2. _**Reword**_ commits(we have already seen one way to do this in [part one](https://bitbucket.org/lakshmanbalaji/lesson1_documentation/src/master/) of this write-up )
3. **_Squash_** commits & **_fix up_** commits.
4. **_Drop_** commits
5. (and [several other things](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History)[^3])

In this section, we will just use the _graphic_ branch to demonstrate just the _fixup_ option of the interative rebase [the others are similar and Git provides verbose options to walk you through the process). Notice that there are 4 unique commits on _graphic_.

![](images/23_interactive1.png)

This level of detail may not be necessary. We are going to use _fixup_ to collapse **_124d, ac12 and zt44_** into _**7d46**_.

1. Ensure _graphic_ is checked out.

2. Type in `git rebase -i HEAD~4` : the `-i` is a flag for `interactive`, and the `HEAD~4` says we want to involve the 4 commits leading up to and including `HEAD` (note that `HEAD` is _**zt44**_)

3. You will see a screen like this.

   ```shell
   pick 7d46 Created basic bar graph of math score by high/low readers
   pick 124d Adding title to bar graph
   pick ac12 changed yaxis label
   pick zt44 added color to bars, changed legend
   
   # Rebase 3681e66..d5f8629 onto 3681e66 (4 commands)
   #
   # Commands:
   # p, pick <commit> = use commit
   # r, reword <commit> = use commit, but edit the commit message
   # e, edit <commit> = use commit, but stop for amending
   # s, squash <commit> = use commit, but meld into previous commit
   # f, fixup [-C | -c] <commit> = like "squash" but keep only the previous
   #                    commit's log message, unless -C is used, in which case
   #                    keep only this commit's message; -c is same as -C but
   #                    opens the editor
   # x, exec <command> = run command (the rest of the line) using shell
   # b, break = stop here (continue rebase later with 'git rebase --continue')
   # d, drop <commit> = remove commit
   # l, label <label> = label current HEAD with a name
   # t, reset <label> = reset HEAD to a label
   # m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
   # .       create a merge commit using the original merge commit's
   # .       message (or the oneline, if no original merge commit was
   # .       specified); use -c <commit> to reword the commit message
   #
   # These lines can be re-ordered; they are executed from top to bottom.
   #
   # If you remove a line here THAT COMMIT WILL BE LOST.
   #
   # However, if you remove everything, the rebase will be aborted.
   #
   
   ```

4.  Press `i` to go into Interactive mode, and then use your keyboard to edit the `pick`. Here, the commits are displayed in the chronological fashion (as opposed to the usual reverse chronological fashion). We want to collapse **_124d, ac12 and zt44_** into _**7d46**_. So, we edit the `pick` to `fixup` for the last three commits (notice bolding)

   ```shell
   pick 7d46 Created basic bar graph of math score by high/low readers
   fixup 124d Adding title to bar graph
   fixup ac12 changed yaxis label
   fixup zt44 added color to bars, changed legend
   
   # Rebase 3681e66..d5f8629 onto 3681e66 (4 commands)
   #
   # Commands:
   # p, pick <commit> = use commit
   # r, reword <commit> = use commit, but edit the commit message
   # e, edit <commit> = use commit, but stop for amending
   # s, squash <commit> = use commit, but meld into previous commit
   # f, fixup [-C | -c] <commit> = like "squash" but keep only the previous
   #                    commit's log message, unless -C is used, in which case
   #                    keep only this commit's message; -c is same as -C but
   #                    opens the editor
   # x, exec <command> = run command (the rest of the line) using shell
   # b, break = stop here (continue rebase later with 'git rebase --continue')
   # d, drop <commit> = remove commit
   # l, label <label> = label current HEAD with a name
   # t, reset <label> = reset HEAD to a label
   # m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
   # .       create a merge commit using the original merge commit's
   # .       message (or the oneline, if no original merge commit was
   # .       specified); use -c <commit> to reword the commit message
   #
   # These lines can be re-ordered; they are executed from top to bottom.
   #
   # If you remove a line here THAT COMMIT WILL BE LOST.
   #
   # However, if you remove everything, the rebase will be aborted.
   #
   ```

   

5. Now hit `Esc` and type `:wq`. You should exit out of the editor and see this message.

```shell
Successfully rebased and updated refs/heads/graphic.
```

This is what the _graphic_ branch looks like now. We have eliminated all the extraneous commits but the work is still preserved.

![](images/24_intrebase.png)



> Note: In a way, this is similar to a squash merge, except we have not done any merging. As pointed out earlier, there are plenty of other options in the interactive rebase. Feel free to explore them to clean up your commit hisotry.

[^1]: This is a dataset from the High School and Beyond survey from the National Center of Education Statistics, released by the [UCLA Institute for Digital Research & Education - Statistical Consulting](https://idre.ucla.edu/resources/stats/statistics-consulting. This dataset has 200 observations and 11 variables: student ID, student gender (male/female), student race (african american, asian, hispanic, white), socioeconomic status (low, middle, high), school type (public, private), program (general, academic, vocational), read (standardized reading score), write (standardized writing score), math (standardized math score), science (standardized science score), socst (standardized social studies score). As denoted by the '_mar_' suffix, this dataset has missignness in certain variables that can be assumed missing at random (MAR)
[^2]: Same dataset as the _hsb2_mar.csv_ dataset, but without any missingness. Can use to check against an ideal scenario where all data is complete. 
[^3]: Chacon and Straub's [Pro Git book](https://git-scm.com/book/en/v2) is a great resource to dive deeper into any of the .